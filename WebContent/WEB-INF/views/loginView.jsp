<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
</head>
<body><center>
	<jsp:include page="_menu.jsp"></jsp:include>   
 
      <h3>Login Page</h3>
 
      <p style="color: red;">${errorString}</p>
 
      <form method="POST" action="${pageContext.request.contextPath}/LoginServlet">
         <input type="hidden" name="redirectId" value="${param.redirectId}" />
         <table border="0">
            <tr>
               <td>User Name</td>
               <td><input type="text" name="userName" value= "${user.userName}" /> </td>
            </tr>
            <tr>
               <td>Password</td>
               <td><input type="password" name="password" value= "${user.password}" /> </td>
            </tr>
          
            <tr>
               <td colspan ="2">
                  <input type="submit" value= "Submit" />
                  <a href="${pageContext.request.contextPath}/HomeServlet">Cancel</a>
               </td>
            </tr>
         </table>
      </form>
 
      <p style="color:blue;">Login with:</p>
       
      
      admin1/123<br>
      saran/saran123<br>
      vimal/vimal123<br>
      jones/jones123<br>
      aravind/aravind123<br>
      akash/akash123<br>
      vimalesh/vimalesh123<br>
      
      </center>
</body>
</html>