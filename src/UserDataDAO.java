import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


public class UserDataDAO {
	private static final Map<String, UserAccount> mapUsers = new HashMap<String, UserAccount>();
	 
	   static {
	      try {
			initUsers();
		} catch (IOException | ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   }

	   private static void initUsers() throws FileNotFoundException, IOException, ClassNotFoundException, SQLException {
	 
	      // This user has a role as AGENT.
		   FetchAgent newFetch = new FetchAgent();
//			newSearch.getUsername();
//			newSearch.getPassword();
			for(int i = 0;i<newFetch.getUsername().size();i++) {
				
				UserAccount name1 = new UserAccount(newFetch.getUsername().get(i),newFetch.getPassword().get(i),UserAccount.GENDER_MALE,SecurityConfig.ROLE_AGENT);
				mapUsers.put(name1.getUserName(), name1);
			}
	      
	 
	      // This user has a role as ADMIN.
	      Properties prop = new Properties();
	      prop.load(UserDataDAO.class.getClassLoader().getResourceAsStream("config.properties"));

			String adminUsername = prop.getProperty("username");
			String adminPassword = prop.getProperty("password");
//			System.out.println(adminPassword);
//			System.out.println(adminUsername);
	      UserAccount admin = new UserAccount(adminUsername, adminPassword, UserAccount.GENDER_MALE, //
	             SecurityConfig.ROLE_ADMIN);
	      mapUsers.put(admin.getUserName(), admin);
	   }
	 
	   // Find a User by userName and password.
	   public static UserAccount findUser(String userName, String password) {
	      UserAccount u = mapUsers.get(userName);
	      if (u != null && u.getPassword().equals(password)) {
	         return u;
	      }
	      return null;
	   }
	
	 
}
