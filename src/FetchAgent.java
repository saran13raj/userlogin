import jdbcConnection.DatabaseConection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;



public class FetchAgent {
	
	public List<String> getUsername() throws ClassNotFoundException, SQLException {
		ResultSet rs = DatabaseConection.connect().executeQuery("select username from agentdata");
		List<String> userNameArray=new ArrayList<>();
		while(rs.next()) {
			
			userNameArray.add(rs.getString("username"));

		}
		return userNameArray;
	}
	public List<String> getPassword() throws ClassNotFoundException, SQLException {
		ResultSet rs = DatabaseConection.connect().executeQuery("select password from agentdata");
		List<String> passwordArray=new ArrayList<>();
		while(rs.next()) {	
			
			passwordArray.add(rs.getString("password"));
			
		}
		return passwordArray;
	}
	
}
