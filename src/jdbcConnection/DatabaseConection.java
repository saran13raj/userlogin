package jdbcConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseConection {

	protected   static Connection initializeDatabase() throws SQLException, ClassNotFoundException {
		String dbDriver = "com.mysql.jdbc.Driver";
		String dbURL = "jdbc:mysql://localhost:3306/";
		
		String dbName = "restaurantcertification";
		String dbUserName = "root";
		String dbPassword = "password";
		
		Class.forName(dbDriver);
		Connection con = DriverManager.getConnection(dbURL + dbName,dbUserName,dbPassword);
		return con;
		
	}
	public static Statement connect() throws ClassNotFoundException, SQLException {
		Connection con = initializeDatabase();
		Statement st=con.createStatement();
		return st;
	}
	

}
